/**
 * 项目相关成员
 *
 * @export
 * @interface IbzProjectMember
 */
export interface IbzProjectMember {

    /**
     * 编号
     *
     * @returns {*}
     * @memberof IbzProjectMember
     */
    id?: any;

    /**
     * 项目名称
     *
     * @returns {*}
     * @memberof IbzProjectMember
     */
    name?: any;

    /**
     * 发布负责人
     *
     * @returns {*}
     * @memberof IbzProjectMember
     */
    rd?: any;

    /**
     * 产品负责人
     *
     * @returns {*}
     * @memberof IbzProjectMember
     */
    po?: any;

    /**
     * 项目负责人
     *
     * @returns {*}
     * @memberof IbzProjectMember
     */
    pm?: any;

    /**
     * 测试负责人
     *
     * @returns {*}
     * @memberof IbzProjectMember
     */
    qd?: any;

    /**
     * 团队成员（一）
     *
     * @returns {*}
     * @memberof IbzProjectMember
     */
    fristmember?: any;

    /**
     * 团队成员（二）
     *
     * @returns {*}
     * @memberof IbzProjectMember
     */
    secondmember?: any;

    /**
     * 团队成员（三）
     *
     * @returns {*}
     * @memberof IbzProjectMember
     */
    thirdmember?: any;

    /**
     * 团队成员（四）
     *
     * @returns {*}
     * @memberof IbzProjectMember
     */
    fourthmember?: any;

    /**
     * 团队成员（五）
     *
     * @returns {*}
     * @memberof IbzProjectMember
     */
    fifthmember?: any;

    /**
     * 团队成员（六）
     *
     * @returns {*}
     * @memberof IbzProjectMember
     */
    sixthmember?: any;
}