/**
 * 系统日志
 *
 * @export
 * @interface Action
 */
export interface Action {

    /**
     * 附加值
     *
     * @returns {*}
     * @memberof Action
     */
    extra?: any;

    /**
     * 对象类型
     *
     * @returns {*}
     * @memberof Action
     */
    objecttype?: any;

    /**
     * id
     *
     * @returns {*}
     * @memberof Action
     */
    id?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof Action
     */
    comment?: any;

    /**
     * 已读
     *
     * @returns {*}
     * @memberof Action
     */
    read?: any;

    /**
     * 动作
     *
     * @returns {*}
     * @memberof Action
     */
    action?: any;

    /**
     * 日期
     *
     * @returns {*}
     * @memberof Action
     */
    date?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Action
     */
    product?: any;

    /**
     * 对象ID
     *
     * @returns {*}
     * @memberof Action
     */
    objectid?: any;

    /**
     * 操作者
     *
     * @returns {*}
     * @memberof Action
     */
    actor?: any;

    /**
     * 项目
     *
     * @returns {*}
     * @memberof Action
     */
    project?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof Action
     */
    lastcomment?: any;
}