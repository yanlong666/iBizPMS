/**
 * 任务团队
 *
 * @export
 * @interface IBZTaskTeam
 */
export interface IBZTaskTeam {

    /**
     * 角色
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    role?: any;

    /**
     * 编号
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    root?: any;

    /**
     * 受限用户
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    limited?: any;

    /**
     * 总计可用
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    total?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    username?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    order?: any;

    /**
     * 可用工日
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    days?: any;

    /**
     * 团队类型
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    type?: any;

    /**
     * 最初预计
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    estimate?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    account?: any;

    /**
     * 总计消耗
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    consumed?: any;

    /**
     * 编号
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    id?: any;

    /**
     * 加盟日
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    join?: any;

    /**
     * 可用工时/天
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    hours?: any;

    /**
     * 预计剩余
     *
     * @returns {*}
     * @memberof IBZTaskTeam
     */
    left?: any;
}