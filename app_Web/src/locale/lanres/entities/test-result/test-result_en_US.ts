
export default {
  fields: {
    lastrunner: "最后执行人",
    stepresults: "步骤结果",
    caseresult: "测试结果",
    xml: "结果文件",
    duration: "持续时间",
    date: "测试时间",
    id: "编号",
    version: "用例版本",
    job: "构建任务",
    ibizcase: "用例",
    run: "测试执行",
    compile: "代码编译",
  },
};