import { ExpViewBase } from './ExpViewBase';

/**
 * 分页导航视图基类
 *
 * @export
 * @class TabExpViewBase
 * @extends {ExpViewBase}
 */
export class TabExpViewBase extends ExpViewBase {

}